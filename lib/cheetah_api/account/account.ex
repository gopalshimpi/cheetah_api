defmodule CheetahApi.Account do
  use CheetahApi.Data

  alias CheetahApi.Account.{User}

  def create(params\\ %{}) do
    %User{}
    |> User.email_changeset(params)
    |> Repo.insert
  end

  def update(params, user) do
    user
    |> User.registration_changeset(params)
    |> Repo.update
  end

  def find(%{"email_address" => email}) do
    User
    |> Repo.get_by(email_address: email)
  end

end
