defmodule CheetahApi.ProfessionalDetails.EducationDetail do
  use CheetahApi.Data

  alias CheetahApi.UserProfile.User

  schema "educational_details" do
    field :institution, :string
    field :degree, :string
    field :start_date, :date
    field :end_date, :date
    field :location, :string

    belongs_to :user, User

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
      |> cast(params, [:institution, :degree, :start_date, :end_date, :location])
      |> validate_required([:institution, :degree, :start_date, :location])
  end

end
