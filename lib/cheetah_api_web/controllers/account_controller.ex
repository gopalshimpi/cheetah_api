defmodule CheetahApiWeb.AccountController do
  use CheetahApiWeb, :controller

  alias CheetahApi.Account
  alias CheetahApi.Account.User

  action_fallback CheetahApiWeb.FallbackController

  def register_email(conn, params) do
    with {:ok , %User{} = user} <- Account.create(params),
         jwt <- Guardian.Plug.api_sign_in(conn, user) |> Guardian.Plug.current_token
      do
        conn
        |> put_status(:created)
        |> put_resp_header("authorization", "Bearer #{jwt}")
        |> render("show.json-api", data: user)
      end
  end

  def create_account(conn, params) do
    with %User{} = user <- params |> Account.find,
         {:ok, user} <- params |> Account.update(user)
    do
      conn |> render("show.json-api", data: user)
    else
      nil -> {:error, :not_found}
      error -> error
    end
  end

end
