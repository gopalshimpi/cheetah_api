defmodule CheetahApiWeb.UserController do
  use CheetahApiWeb, :controller

  alias CheetahApi.UserProfile.User
  alias CheetahApi.UserProfile

  def show(conn, params) do
    with %User{} = user <-
      UserProfile.find(params)
    do
      conn |> render("show.json-api", data: user)
    end
  end

end