defmodule CheetahApiWeb.UserView do
  use CheetahApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :id, :first_name, :last_name, :email_address, :mobile_number
  ]

end
