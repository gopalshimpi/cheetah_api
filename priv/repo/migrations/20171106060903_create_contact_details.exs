defmodule CheetahApi.Repo.Migrations.CreateContactDetails do
  use Ecto.Migration

  def change do
    create table(:contact_details) do
      add :primary_email, :string
      add :secondary_email, :string
      add :website_url, :string

      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:contact_details, [:user_id])
    create unique_index(:contact_details, [:primary_email])
  end
end
