defmodule CheetahApi.Repo.Migrations.WorkExperienceProfessionalRoles do
  use Ecto.Migration

  def change do
    create table(:work_experience_professional_roles) do
      add :work_experience_id, references(:work_experiences, on_delete: :delete_all)
      add :professional_role_id, references(:professional_roles)
    end

    create index(:work_experience_professional_roles, [:work_experience_id])
    create index(:work_experience_professional_roles, [:professional_role_id])
    create unique_index(:work_experience_professional_roles, [:work_experience_id, :professional_role_id])
  end
end
